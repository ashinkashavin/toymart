
package TOYMARTCLASS;

import java.io.BufferedReader;
import TOYMARTCLASS.FILEClass;
/**
 *
 * @author ashinka
 */
public class toymartempclass {

        FILEClass fs =  new FILEClass("ToymartEmployee.txt");
    
    private String EmployeeId;
    private String Employeename;
    private int age;
    private String Address;
    private String Department;

    public toymartempclass(){
    }
    
    
    public toymartempclass(String EmployeeId, String Employeename, int age, String Address, String Department) {
        this.EmployeeId = EmployeeId;
        this.Employeename = Employeename;
        this.age = age;
        this.Address = Address;
        this.Department = Department;
        
    }
    
    
    private int Employeecount;

    public String getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(String EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getEmployeename() {
        return Employeename;
    }

    public void setEmployeename(String Employeename) {
        this.Employeename = Employeename;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public int getEmployeecount() {
        return Employeecount;
    }

    public void setEmployeecount(int Employeecount) {
        this.Employeecount = Employeecount;
    }
    
       public boolean addEmployee()
    {
       if (!fs.createANewFile())
       {
         String record = EmployeeId+ " "+Employeename+" "+age+" "+Address+" "+Department;
           System.err.println(EmployeeId+ " "+Employeename+" "+age+" "+Address+" "+Department);
           return fs.writeDataToFile(record);
       }
       return false;
    }
    
       public boolean searchEmployee (String EmployeeId)
{
boolean isFound = false;
try {
String[] words = null;
BufferedReader emp = fs.readAFile();
String employee;
outerloop:
while ((employee = emp.readLine()) != null) {
words = employee.split(" ");
for (String word : words) {
if (word.equals(EmployeeId))
{
isFound = true;
this.setEmployeeId(words[0]);
this.setEmployeename(words[1]);
this.setAge(Integer.parseInt(words[2]));
this.setAddress(words[3]);
this.setDepartment(words[4]);
break outerloop;
}
}
}
} catch (Exception e) {
}
return isFound;
}
       public String viewAllEmployee ()
{
String Employee, allEmployee = "  ";
String[] words = null;
int count = 0;

 BufferedReader emp = fs.readAFile();

 try {
while ((Employee = emp.readLine()) != null)
{
words = Employee.split(" ");
allEmployee = allEmployee + words[0] + "\t" + words[1] + "\t" + words[2] + "\t" + words[3] + "\n";
count++;
}
} catch (Exception e) {
    System.err.println("Error searching Emp"+e);
}

 setEmployeecount(count);
return allEmployee;
}

     
}


