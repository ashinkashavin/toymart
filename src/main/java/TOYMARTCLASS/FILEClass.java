/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TOYMARTCLASS;


import java.io.*;

public class FILEClass {
private static final String FILE_PATH = "D:\\FILES\\";

File file;
private String fileName;


public FILEClass(String fileName){
this.fileName=fileName;

createANewFile();

}
public boolean createANewFile(){
try{
file=new File(FILE_PATH + fileName);
if(file.createNewFile()){
System.out.println("File Created:" +file.getName());
return true;
}
    System.out.println("File already exsist!");
    return false;
}catch(IOException e){
System.out.println("something went wrong with creating file " +e);
return false;
    
}

}

public boolean writeDataToFile(String record){
try{
file.createNewFile();
FileWriter newWriter = new FileWriter(file, true);
BufferedWriter newBuffer = new BufferedWriter(newWriter);

newBuffer.write(record);
newBuffer.newLine();
newBuffer.close();
newWriter.close();

return true;

}catch(IOException e){
System.out.println("something went wrong with writing file " +e);
return false;
    
}

}

public BufferedReader readAFile(){
if(!createANewFile())
{
try{
FileReader reader = new FileReader(file);
BufferedReader buffer = new BufferedReader(reader);

return buffer;
}catch(IOException e){
System.out.println("something went wrong with reading file " +e);
}
}
return null;
}
    
}
