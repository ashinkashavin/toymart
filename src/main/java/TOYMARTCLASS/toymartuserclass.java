/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TOYMARTCLASS;

import java.io.BufferedReader;




public class toymartuserclass {
    
    FILEClass fs = new FILEClass("toymartusers.txt");
    
    
    private String userid;
    private String password;
    private String role;
    private int usercount;

    public toymartuserclass(String userid, String password, String role) {
        this.userid = userid;
        this.password = password;
        this.role = role;
        
    }

    public toymartuserclass(String userid, String password) {
        this.userid = userid;
        this.password = password;
    }

    public toymartuserclass(String userid, String password, String role, int usercount) {
        this.userid = userid;
        this.password = password;
        this.role = role;
        this.usercount = usercount;
    }

    

    
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getUsercount() {
        return usercount;
    }

    public void setUsercount(int usercount) {
        this.usercount = usercount;
    }
    
    public boolean addNewUser(){
    if(!fs.createANewFile()){
        String record = userid+"   "+password+"    "+role;
        
        return fs.writeDataToFile(record);
        
    }
    return false;
    
    }
 
   public boolean validateLogin() 
    {
        try {
            String[] words = null;
            BufferedReader users = fs.readAFile();
            String user;

            while ((user = users.readLine()) != null) {
                words = user.split("   ");
            }
            if (words[0].equals(userid) && words[1].equals(password)) {
                this.setUserid(words[0]);
                this.setPassword(words[1]);
                this.setRole(words[2]);
                return true;
            }
        } 
        catch (Exception e) 
        {}

        return false;
    }
     public String viewAllUsers ()
{
String User, allUsers = " ";
String[] words = null;
int count = 0;

 BufferedReader user = fs.readAFile();

 try {
while ((User = user.readLine()) != null) {
words = User.split(" ");
allUsers = allUsers + words[0] + "\t" + words[2] + "\n";
count++;
}
} catch (Exception e) {
}
setUsercount(count);
return allUsers;
}  
   
}
    